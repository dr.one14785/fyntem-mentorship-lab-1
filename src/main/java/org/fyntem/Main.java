package org.fyntem;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter first number:");
        int n1 = scanner.nextInt();
        System.out.println("... and now second number:");
        int n2 = scanner.nextInt();

        System.out.println("Summa: " + addTwoNumbers(n1, n2));
        System.out.println("Riznytsya: " + subtractTwoNumbers(n1, n2));
        System.out.println("Mnolennya: " + multiplyTwoNumbers(n1, n2));
    }

    public static int addTwoNumbers(int a1, int a2) {
        int sum = a1 + a2;
        return sum;
    }

    public static int subtractTwoNumbers(int a1, int a2) {
        int result = 0;
        if (a1 > a2) {
            result = a1 - a2;
        } else if (a1 < a2) {
            result = a2 - a1;
        } else { // (a1 == a2)
            result = 0;
        }
        return result;
//        return Math.abs(a1 - a2);
    }

    public static int multiplyTwoNumbers(int a1, int a2) {
        int mnojennya = a1 * a2;
        return mnojennya;
    }


}